import { useAppSelector } from 'hooks/redux';
import { favouritePosts } from 'features/posts/postsSlice';
import SinglePost from 'components/SinglePost';

const FavouritesPage = () => {
  const posts = useAppSelector(favouritePosts);

  return (
    <section>
      <div className='container'>
        <h1 className='my-10 font-bold text-3xl text-obsidian-900'>
          Your favourite posts
        </h1>

        <div className='grid grid-cols-3 gap-10'>
          {posts.map((post) => (
            <SinglePost key={post.id} post={post} />
          ))}
        </div>
      </div>
    </section>
  );
};

export default FavouritesPage;
