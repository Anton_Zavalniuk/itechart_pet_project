import { useState } from 'react';
import { BsPlusCircleFill } from 'react-icons/bs';

import { useAppSelector } from 'hooks/redux';
import SinglePost from 'components/SinglePost';
import Modal from 'components/Modal';
import CreateEditPostModal from 'components/Modal/CreateEditPostModal';

const HomePage = () => {
  const posts = useAppSelector((state) => state.posts);
  const [isCreatePostModalOpen, setIsCreatePostModalOpen] = useState(false);

  const toggleCreatePostModal = () => {
    setIsCreatePostModalOpen((prevState) => !prevState);
  };

  return (
    <section className='relative'>
      <button
        className='absolute top-0 right-8 text-amber-500 hover:text-amber-600'
        onClick={toggleCreatePostModal}
      >
        <BsPlusCircleFill fontSize={42} />
      </button>

      <div className='container'>
        <h1 className='my-10 font-bold text-3xl text-obsidian-900'>
          Your posts
        </h1>

        <div className='grid grid-cols-3 gap-10'>
          {posts.map((post) => (
            <SinglePost key={post.id} post={post} />
          ))}
        </div>
      </div>

      <Modal
        isOpen={isCreatePostModalOpen}
        onClose={toggleCreatePostModal}
        title='Create new post'
      >
        <CreateEditPostModal onClose={toggleCreatePostModal} />
      </Modal>
    </section>
  );
};

export default HomePage;
