import LoginForm from 'components/LoginForm';

const LoginPage = () => {
  return (
    <section className='flex items-center justify-center min-h-screen bg-gray-50'>
      <div className='flex items-center w-1/2 p-5 rounded-2xl bg-gray-100 shadow-lg'>
        <div className='w-1/2 px-16'>
          <h1 className='font-bold text-2xl text-obsidian-900'>Login</h1>

          <p className='mt-4 text-xs text-obsidian-900'>
            If you are already a member, easily log in
          </p>

          <LoginForm />
        </div>

        <div className='w-1/2'>
          <img
            src='https://images.unsplash.com/photo-1543599538-a6c4f6cc5c05?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80'
            alt='welcome back image'
            className='rounded-2xl'
          />
        </div>
      </div>
    </section>
  );
};

export default LoginPage;
