import { createSlice } from '@reduxjs/toolkit';

import { RootState } from 'store/index';
import { Post } from 'interfaces/post.interface';

const initialState: Post[] = [
  {
    id: '1668771198262',
    title: 'First post',
    description: 'Short description of the first post',
    createdAt: '18.11.2022',
    isFavourite: false,
  },
  {
    id: '1668771198275',
    title: 'Second post',
    description: 'Short description of the second post',
    createdAt: '18.11.2022',
    isFavourite: false,
  },
  {
    id: '1668771198276',
    title: 'Third post',
    description: 'Short description of the third post',
    createdAt: '18.11.2022',
    isFavourite: false,
  },
];

export const postsSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {
    addNewPost: (state, { payload }) => [...state, payload],
    editPost: (state, { payload }) =>
      state.map((post) => {
        if (post.id === payload.id) {
          return payload;
        }

        return post;
      }),
    deletePost: (state, { payload }) =>
      state.filter((post) => post.id !== payload),
    addToFavourite: (state, { payload }) =>
      state.map((post) => {
        if (post.id === payload) {
          return {
            ...post,
            isFavourite: !post.isFavourite,
          };
        }

        return post;
      }),
  },
});

export const { addNewPost, editPost, deletePost, addToFavourite } =
  postsSlice.actions;

export const favouritePosts = (state: RootState) =>
  state.posts.filter((post) => post.isFavourite);

export default postsSlice.reducer;
