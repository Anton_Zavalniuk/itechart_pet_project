import { createSlice } from '@reduxjs/toolkit';

import { Auth } from 'interfaces/auth.interface';

const initialState: Auth = {
  isAuth: false,
  user: {
    email: '',
    password: '',
  },
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login: (state, { payload }) => {
      state.isAuth = true;
      state.user.email = payload.email;
      state.user.password = payload.password;
    },
    logout: (state) => {
      state.isAuth = false;
      state.user.email = '';
      state.user.password = '';
    },
  },
});

export const { login, logout } = authSlice.actions;
export default authSlice.reducer;
