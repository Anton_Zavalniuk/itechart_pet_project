import { useState } from 'react';
import { Formik } from 'formik';
import { AiOutlineEyeInvisible, AiOutlineEye } from 'react-icons/ai';
import { useNavigate } from 'react-router-dom';

import { User } from 'interfaces/user.interface';
import { useAppDispatch } from 'hooks/redux';
import { login } from 'features/auth/authSlice';

const initialValues: User = {
  email: '',
  password: '',
};

const LoginForm = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);

  const togglePasswordVisibility = () => {
    setIsPasswordVisible((prevState) => !prevState);
  };

  const onSubmit = (values: User) => {
    if (values.email && values.password) {
      dispatch(login(values));
      navigate('home');
    }
  };

  return (
    <Formik initialValues={initialValues} onSubmit={onSubmit}>
      {({ values, handleChange, handleSubmit }) => (
        <form className='flex flex-col gap-4' onSubmit={handleSubmit}>
          <input
            type='email'
            placeholder='Email'
            name='email'
            onChange={handleChange}
            value={values.email}
            className='p-2 mt-8 rounded-xl border'
          />

          <div className='relative'>
            <input
              type={isPasswordVisible ? 'text' : 'password'}
              placeholder='Password'
              name='password'
              onChange={handleChange}
              value={values.password}
              className='w-full p-2 rounded-xl border'
            />

            <button
              type='button'
              className='absolute top-1/2 right-3 -translate-y-1/2 hover:[&_svg]:text-red-500'
              onClick={togglePasswordVisibility}
            >
              {isPasswordVisible ? <AiOutlineEye /> : <AiOutlineEyeInvisible />}
            </button>
          </div>

          <button
            disabled={!values.email || !values.password}
            type='submit'
            className='rounded-xl text-white py-2 bg-obsidian-900 hover:bg-obsidian-100 duration-300 disabled:bg-gray-300 disabled:cursor-not-allowed'
          >
            Login
          </button>
        </form>
      )}
    </Formik>
  );
};

export default LoginForm;
