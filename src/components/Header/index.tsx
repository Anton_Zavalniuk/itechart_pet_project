import { NavLink, Link } from 'react-router-dom';
import { ImExit } from 'react-icons/im';

import { ROUTES } from 'routes';
import { logout } from 'features/auth/authSlice';
import { useAppDispatch, useAppSelector } from 'hooks/redux';

const Header = () => {
  const dispatch = useAppDispatch();
  const user = useAppSelector((state) => state.auth.user.email);

  const handleLogoutClick = () => {
    dispatch(logout());
  };

  return (
    <header className='flex items-center h-16 px-8 bg-obsidian-900'>
      <Link to='/home'>
        <span className='font-bold text-2xl text-white'>My Logo</span>
      </Link>

      <nav className='flex items-center ml-auto'>
        <ul className='flex items-center'>
          {ROUTES.map((route) => (
            <li
              key={route.name}
              className='font-semibold text-white ml-3 first:ml-0'
            >
              <NavLink
                to={route.path}
                className={({ isActive }) =>
                  isActive ? 'active-link' : undefined
                }
              >
                {route.name}
              </NavLink>
            </li>
          ))}
        </ul>
      </nav>

      <span className='ml-20 text-white'>{user}</span>

      <button
        className='ml-10 text-white hover:text-red-500'
        onClick={handleLogoutClick}
      >
        <ImExit fontSize={24} />
      </button>
    </header>
  );
};

export default Header;
