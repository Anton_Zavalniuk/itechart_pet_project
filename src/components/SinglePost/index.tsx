import { BiEdit, BiLike } from 'react-icons/bi';
import { MdDelete } from 'react-icons/md';
import { useState } from 'react';

import { Post } from 'interfaces/post.interface';
import { deletePost, addToFavourite } from 'features/posts/postsSlice';
import Modal from 'components/Modal';
import CreateEditPostModal from 'components/Modal/CreateEditPostModal';
import { useAppDispatch } from 'hooks/redux';

interface Properties {
  post: Post;
}

const SinglePost = ({ post }: Properties) => {
  const dispatch = useAppDispatch();
  const [isEditPostModalOpen, setIsEditPostModalOpen] = useState(false);

  const toggleEditPostModal = () => {
    setIsEditPostModalOpen((prevState) => !prevState);
  };

  const onDeleteClick = () => {
    dispatch(deletePost(post.id));
  };

  const onLikeClick = () => {
    dispatch(addToFavourite(post.id));
  };

  return (
    <>
      <article key={post.id} className='p-5 rounded-2xl bg-gray-100 shadow-sm'>
        <h2 className='mb-4 font-bold text-lg underline underline-offset-2'>
          {post.title}
        </h2>
        <p className='line-clamp-1 mb-3'>{post.description}</p>

        <div className='flex items-center'>
          <span className='text-sm'>{post.createdAt}</span>

          <button
            className={`ml-auto ${
              post.isFavourite ? 'text-green-700' : 'text-gray-700'
            }`}
            onClick={onLikeClick}
          >
            <BiLike fontSize={24} />
          </button>

          <button
            className='text-indigo-700 ml-2'
            onClick={toggleEditPostModal}
          >
            <BiEdit fontSize={24} />
          </button>

          <button className='text-red-700 ml-2' onClick={onDeleteClick}>
            <MdDelete fontSize={24} />
          </button>
        </div>
      </article>

      <Modal
        isOpen={isEditPostModalOpen}
        onClose={toggleEditPostModal}
        title='Edit post'
      >
        <CreateEditPostModal
          onClose={toggleEditPostModal}
          data={post}
          isEditMode
        />
      </Modal>
    </>
  );
};

export default SinglePost;
