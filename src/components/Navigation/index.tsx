import { Routes, Route } from 'react-router-dom';

import HomePage from 'pages/HomePage';
import FavouritesPage from 'pages/FavouritesPage';
import LoginPage from 'pages/LoginPage';

import ProtectedRoute from './ProtectedRoute';

const Navigation = () => {
  return (
    <Routes>
      <Route index element={<LoginPage />} />
      <Route
        path='home'
        element={
          <ProtectedRoute>
            <HomePage />
          </ProtectedRoute>
        }
      />
      <Route
        path='favourites'
        element={
          <ProtectedRoute>
            <FavouritesPage />
          </ProtectedRoute>
        }
      />
    </Routes>
  );
};

export default Navigation;
