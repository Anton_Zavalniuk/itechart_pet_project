import { Navigate, useLocation } from 'react-router-dom';

import { useAppSelector } from 'hooks/redux';

interface Properties {
  children: JSX.Element;
}

const ProtectedRoute = ({ children }: Properties) => {
  const isAuth = useAppSelector((state) => state.auth.isAuth);
  const location = useLocation();

  return isAuth ? (
    children
  ) : (
    <Navigate to='/' state={{ from: location }} replace />
  );
};

export default ProtectedRoute;
