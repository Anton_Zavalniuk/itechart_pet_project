import { Formik } from 'formik';
import { nanoid } from 'nanoid';
import dayjs from 'dayjs';

import { Post } from 'interfaces/post.interface';
import { addNewPost, editPost } from 'features/posts/postsSlice';
import { useAppDispatch } from 'hooks/redux';

interface Properties {
  onClose: () => void;
  isEditMode?: boolean;
  data?: Post;
}

const CreateEditPostModal = ({ onClose, isEditMode, data }: Properties) => {
  const dispatch = useAppDispatch();

  const initialValues: Omit<Post, 'createdAt' | 'id' | 'isFavourite'> = {
    title: data && isEditMode ? data.title : '',
    description: data && isEditMode ? data.description : '',
  };

  const onSubmit = (values: Omit<Post, 'createdAt' | 'id' | 'isFavourite'>) => {
    const post = {
      title: values.title,
      description: values.description,
      id: isEditMode ? data?.id : nanoid(),
      createdAt: isEditMode ? data?.createdAt : dayjs().format('DD-MM-YYYY'),
      isFavourite: isEditMode ? data?.isFavourite : false,
    };
    dispatch(isEditMode ? editPost(post) : addNewPost(post));
    onClose();
  };

  return (
    <div className='w-[30vw] p-6'>
      <Formik initialValues={initialValues} onSubmit={onSubmit}>
        {(props) => (
          <form className='flex flex-col gap-4' onSubmit={props.handleSubmit}>
            <input
              type='text'
              placeholder='Enter post title...'
              name='title'
              onChange={props.handleChange}
              value={props.values.title}
              className='p-2 rounded-xl border'
            />
            <input
              type='text'
              placeholder='Enter post description...'
              name='description'
              onChange={props.handleChange}
              value={props.values.description}
              className='p-2 rounded-xl border'
            />

            <button
              type='submit'
              className='rounded-xl text-white py-2 bg-obsidian-900 hover:bg-obsidian-100 duration-300'
            >
              {isEditMode ? 'Edit post' : 'Create post'}
            </button>
          </form>
        )}
      </Formik>
    </div>
  );
};

export default CreateEditPostModal;
