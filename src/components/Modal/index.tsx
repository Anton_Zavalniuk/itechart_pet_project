import { MouseEvent, useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import { IoMdClose } from 'react-icons/io';

interface Properties {
  isOpen: boolean;
  onClose: () => void;
  title: string;
  children: React.ReactNode;
}

const Modal = ({ isOpen, onClose, title, children }: Properties) => {
  const [isBrowser, setIsBrowser] = useState(false);

  const onCloseClick = (event: MouseEvent<HTMLButtonElement>) => {
    event?.preventDefault();
    onClose();
  };

  useEffect(() => {
    setIsBrowser(true);
  }, []);

  const modelContent = isOpen ? (
    <div className='fixed top-0 left-0 right-0 bottom-0 w-full h-full flex justify-center items-center bg-backdrop'>
      <div className='w-auto h-auto rounded-2xl bg-white z-50'>
        <div className='flex justify-between items-center py-3 px-6 border-b-gray-300 border-b'>
          <h3 className='text-2xl'>{title}</h3>
          <button
            className='text-2xl text-gray-500 hover:text-red-500'
            onClick={onCloseClick}
          >
            <IoMdClose />
          </button>
        </div>
        {children}
      </div>
    </div>
  ) : null;

  return isBrowser
    ? ReactDOM.createPortal(
        modelContent,
        document.getElementById('modal-root') as HTMLElement
      )
    : null;
};

export default Modal;
