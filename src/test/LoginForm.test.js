import { screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';
import '@testing-library/jest-dom';

import LoginForm from 'components/LoginForm';
import renderWithProviders from 'utils/testUtils';

describe('Login form', () => {
  beforeEach(() => renderWithProviders(<LoginForm />));

  it('login button is disabled when init render', async () => {
    expect(
      await screen.findByRole('button', { name: /login/i })
    ).toBeDisabled();
  });

  it('login button is enabled when form fields are filled', async () => {
    const emailField = screen.getByPlaceholderText('Email');
    const passwordField = screen.getByPlaceholderText('Password');

    waitFor(() => {
      userEvent.type(emailField, 'anton@gmail.com');
      userEvent.type(passwordField, '123456');
    });

    expect(emailField).toHaveValue('anton@gmail.com');
    expect(passwordField).toHaveValue('123456');

    expect(await screen.findByRole('button', { name: /login/i })).toBeEnabled();
  });

  it('form submitting', () => {
    const onSubmit = jest.fn();
    const emailField = screen.getByPlaceholderText('Email');
    const passwordField = screen.getByPlaceholderText('Password');
    const submitButton = screen.getByRole('button', { name: /login/i });

    userEvent.type(emailField, 'anton@gmail.com');
    userEvent.type(passwordField, '123456');

    userEvent.click(submitButton);

    waitFor(() => {
      expect(onSubmit).toHaveBeenCalledWith({
        email: 'anton@gmail.com',
        password: '123456',
      });
    });
  });
});
