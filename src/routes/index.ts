export const ROUTES = [
  {
    name: 'Home',
    path: 'home',
  },
  {
    name: 'Favourites',
    path: 'favourites',
  },
];
