import { useLocation } from 'react-router-dom';

import Header from 'components/Header';
import Navigation from 'components/Navigation';

const App = () => {
  const { pathname } = useLocation();

  return (
    <div>
      {pathname !== '/' && <Header />}

      <Navigation />
    </div>
  );
};

export default App;
