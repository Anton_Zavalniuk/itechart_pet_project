import { User } from 'interfaces/user.interface';

export interface Auth {
  isAuth: boolean;
  user: User;
}
