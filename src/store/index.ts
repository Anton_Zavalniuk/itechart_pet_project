import { configureStore } from '@reduxjs/toolkit';

import authSlice from 'features/auth/authSlice';
import postsSlice from 'features/posts/postsSlice';

export const store = configureStore({
  reducer: {
    auth: authSlice,
    posts: postsSlice,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export type AppStore = typeof store;
