/** @type {import('tailwindcss').Config} */

module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  darkMode: 'class',
  theme: {
    extend: {
      colors: {
        'obsidian-900': '#002d74',
        'obsidian-100': '#1e6ace',
        backdrop: 'rgba(0,0,0,0.5)',
      },
    },
  },
  plugins: [require('@tailwindcss/line-clamp')],
};
